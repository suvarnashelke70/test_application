import 'dart:convert';

import 'package:crud_demo/model/post.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;

class PostController {
  String baseUrl = "https://jsonplaceholder.typicode.com/";

  Future<int> createPost(Post post) async {
    try {
      String url = baseUrl + "posts";
      Map<String, dynamic> mapBody = {
        "title": post.title,
        "body": post.body,
        "userId": post.userId,
      };
      http.Response response =
          await http.post(Uri.parse(url), body: json.encode(mapBody));

      if (response.statusCode == 201) {
        Map<String, dynamic> map = jsonDecode(response.body);
        return map["id"];
      } else {
        return 0;
      }
    } catch (e) {
      return 0;
    }
  }

  Future<Map<String, dynamic>> getPostsList() async {
    Map<String, dynamic> resObj = {};
    try {
      String url = baseUrl + "posts";
      http.Response response = await http.get(Uri.parse(url));

      if (response.statusCode == 200) {
        List<Post> posts = [];
        var responseData = json.decode(response.body);

        responseData.forEach((element) {
          Post post = Post.fromJson(element);
          posts.add(post);
        });

        resObj["status"] = 1;
        resObj["posts"] = posts;

        //  List<Post> posts1 = (responseData as List).map((e) => Post.fromJson(e)).toList();
        //   print(posts);
      } else {
        resObj["status"] = 0;
      }
    } catch (e) {
      resObj["status"] = 0;
      print(e);
    }
    return resObj;
  }

  Future<bool> deletePost(int id) async {
    String url = baseUrl + "posts/" + id.toString();

    var response = await http.delete(Uri.parse(url));
    if (response.statusCode == 200) {
      EasyLoading.showSuccess("Data deleted successfully");
      return true;
    } else {
      return false;
    }
  }

  Future<int> updatePost(Post post) async {
    try {
      String url = baseUrl + "posts/" + post.id.toString();
      Map<String, dynamic> mapBody = {
        "title": post.title,
        "body": post.body,
        "userId": post.userId,
        "id": post.id,
      };
      http.Response response =
          await http.put(Uri.parse(url), body: json.encode(mapBody));

      if (response.statusCode == 200) {
        Map<String, dynamic> map = jsonDecode(response.body);
        return map["id"];
      } else {
        return 0;
      }
    } catch (e) {
      return 0;
    }
  }
}
