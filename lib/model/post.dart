import 'package:flutter/cupertino.dart';

class Post {
  String title;
  String body;
  int id;
  int userId;

  Post({@required this.body, @required this.title, this.id, this.userId});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      title: json['title'],
      userId: json['userId'],
      id: json['id'],
      body: json['body'],
    );
  }
}
