import 'package:crud_demo/controller/post_controller.dart';
import 'package:crud_demo/model/post.dart';
import 'package:crud_demo/screens/dashboard.dart';
import 'package:flutter/material.dart';

import 'package:flutter_easyloading/flutter_easyloading.dart';

class CreateScreen extends StatefulWidget {
  final Post post;
  const CreateScreen({Key key, this.post}) : super(key: key);

  @override
  _CreateScreenState createState() => _CreateScreenState();
}

class _CreateScreenState extends State<CreateScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _titleEditingController = new TextEditingController();
  TextEditingController _bodyEditingController = new TextEditingController();
  PostController _postController = new PostController();

  @override
  void initState() {
    super.initState();
    if (widget.post != null) {
      _titleEditingController.text = widget.post.title;
      _bodyEditingController.text = widget.post.body;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Dashboard()),
            );
          },
          child: Icon(Icons.navigate_before),
        ),
        appBar: AppBar(
          title: Text(widget.post == null ? "Create Post" : "Update Post"),
        ),
        body: Container(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                    child: TextFormField(
                      maxLines: 2,
                      controller: _titleEditingController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please Enter Title';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          hintText: "Enter Title",
                          fillColor: Colors.white,
                          focusColor: Colors.white),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                    child: TextFormField(
                      maxLines: 8,
                      controller: _bodyEditingController,
                      decoration: InputDecoration(
                          hintText: "Please Enter Body",
                          fillColor: Colors.grey,
                          focusColor: Colors.grey),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please Enter body';
                        }
                        return null;
                      },
                    ),
                  ),
                ),
                widget.post == null
                    ? TextButton(
                        onPressed: () async {
                          EasyLoading.show();
                          bool isValid = _formKey.currentState.validate();
                          if (isValid) {
                            Post post = new Post(
                                body: _bodyEditingController.text,
                                title: _titleEditingController.text,
                                userId: 1);
                            int id = await _postController.createPost(post);
                            EasyLoading.dismiss();
                            if (id > 0) {
                              EasyLoading.showSuccess("successfully created");
                            } else {
                              EasyLoading.showToast("Something went wrong");
                            }
                          }
                          _bodyEditingController.clear();
                          _titleEditingController.clear();
                        },
                        style: TextButton.styleFrom(
                          primary: Colors.black,
                        ),
                        child: Text(
                          "Save",
                          style: TextStyle(fontSize: 30),
                        ))
                    : TextButton(
                        onPressed: () async {
                          EasyLoading.show();
                          Post post = new Post(
                              body: _bodyEditingController.text,
                              title: _titleEditingController.text,
                              id: widget.post.id,
                              userId: 1);
                          int id = await _postController.updatePost(post);
                          EasyLoading.dismiss();
                          if (id > 0) {
                            EasyLoading.showSuccess("successfully Updated");
                          } else {
                            EasyLoading.showToast("Something went wrong");
                          }
                        },
                        child: Text(
                          "Update",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ))
              ],
            ),
          ),
        ));
  }
}
