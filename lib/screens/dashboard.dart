import 'package:crud_demo/controller/post_controller.dart';
import 'package:crud_demo/model/post.dart';
import 'package:crud_demo/screens/create_screen.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  PostController _postController = new PostController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
      ),
      body: Column(
        children: [
          Row(
            children: [
              TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => (CreateScreen())));
                  },
                  child: Text(
                    "Create Post",
                    style: TextStyle(fontSize: 18.0),
                  )),
            ],
          ),
          Flexible(
            child: Container(
              child: FutureBuilder(
                future: _postController.getPostsList(),
                builder: (BuildContext context,
                    AsyncSnapshot<Map<String, dynamic>> snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data["status"] == 1) {
                      List<Post> posts = snapshot.data["posts"];
                      return ListView.builder(
                        shrinkWrap: true,
                        padding: EdgeInsets.zero,
                        itemCount: posts.length,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Text(posts[index].title),
                                      content: SingleChildScrollView(
                                          child: Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.3,
                                              child: Text(posts[index].body))),
                                    );
                                  });
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.grey[50],
                                      border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20.0, right: 20.0),
                                    child: Center(
                                        child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            width: 200,
                                            child: Text(
                                              posts[index].title,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          GestureDetector(
                                              onTap: () async {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            CreateScreen(
                                                              post:
                                                                  posts[index],
                                                            )));
                                              },
                                              child: Icon(Icons.edit)),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          GestureDetector(
                                              onTap: () async {
                                                await _postController
                                                    .deletePost(
                                                        posts[index].id);
                                              },
                                              child: Icon(Icons.delete))
                                        ],
                                      ),
                                    )),
                                  )),
                            ),
                          );
                        },
                      );
                    } else {
                      return Container();
                    }
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
